const path = require('path')
const webpack = require('webpack')
const Dotenv = require('dotenv-webpack');


const ENVIRONMENT = process.env.ENVIRONMENT;
console.log("webpack:", ENVIRONMENT);

if (!['development', 'production'].includes(ENVIRONMENT)) {
  throw Error('-->> Set ENVIRONMENT variable <<--')
};

module.exports = {
  output: {
    filename: `worker.js`,
    path: path.join(__dirname, "dist"),
  },
  target: 'webworker',
  mode: ENVIRONMENT,
  entry: "./cf.js",
  resolve: {
    alias: {
      //? cf /upload syncing
      fs: path.resolve(__dirname, "./src/null.js"),
      child_process: path.resolve(__dirname, "./src/null.js"),
    },
  },
  plugins: [
    new Dotenv({
      path: `./.env.${ENVIRONMENT}`
    }),
  ],
};
