const path = require('path')
const webpack = require('webpack')
const Dotenv = require('dotenv-webpack');

const ENVIRONMENT = 'production' || process.env.ENVIRONMENT;
console.log("node webpack:", ENVIRONMENT);


module.exports = {
  output: {
    filename: `built-time.js`,
    path: path.join(__dirname, "dist"),
  },
  target: 'node',
  mode: ENVIRONMENT,
  entry: "./server.js",
  plugins: [
    new Dotenv({
      path: `./.env.${ENVIRONMENT}`
    }),
    new webpack.optimize.LimitChunkCountPlugin({
      maxChunks: 1
    })
  ],
};
