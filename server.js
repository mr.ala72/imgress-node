const ENVIRONMENT = process.env.ENVIRONMENT || "development";
const isProduction = ENVIRONMENT === 'production';
console.log({ENVIRONMENT});
require('dotenv').config({
    path: `./.env.${ENVIRONMENT}`
});


const express = require('express');
const morgan = require('morgan')
const multer  = require('multer');
const cors = require('cors');
const faunadb = require('faunadb');
const q = faunadb.query;
const nanoid = require('nanoid').customAlphabet('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz', 19);
const { getGoogleUser } = require('./src/utils');
const md5 = require('./src/lib/md5');
const tagImages = require('./src/logic/image-tagging');
const upload = multer();


//? Express
const port = process.env.PORT || 3000;
const app = express();

isProduction && app.use(morgan('combined'));
app.use(express.json({limit: '5mb', strict: false}));
app.use(express.urlencoded({limit: '5mb', parameterLimit:5000, extended:true}));
app.use(cors({
    origin: ['https://imgress.com', 'http://localhost:3301'],
    maxAge: 86400
}));

//? Fauna Client for requests
function faunaClient ( req ) {
    const token = process.env.FAUNA_SECRET //req.query.token;
    return {
        client: new faunadb.Client({ 
            secret: token,
            domain: process.env.FAUNA_DOMAIN,
            port: process.env.FAUNA_PORT,
            scheme: process.env.FAUNA_SCHEME,
            timeout: 3000 
        })
    }
};

//? Node compatible respose
function translator ({ statusCode, body }, callback) {
    if (!callback) throw Error('res is not passed!') 
    callback.status(statusCode).json(body);
}

//! ----------------------------------------------------------------
//! ----------------------------------------------------------------
//! ----------------------------------------------------------------
//! ----------------------------------------------------------------
//! ----------------------------------------------------------------

app.get('/', (req, res)=> res.send('You may wanto check imgress.com!'))

//? all routes
const routes = require('./src/core')(faunaClient, translator);

//? multer form-data parsing for these routes
app.all(
    [
        '/updatevote', 
        '/castvote', 
        '/createuser', 
        '/edituser', 
        '/createballot', 
        '/createanonymous', 
        '/anonymousballotsupdatevote', 
        '/anonymousballotsvote',
        '/pushnotif',
        '/savepush',
        '/ballots-by-tags'
    ], 
    upload.none()
);

//? registering paths as express routes
for(const [path, handler] of Object.entries(routes)) {
    app.all(path, handler)
}


app.get("/pushnotif", async (req, res) => {
    const { userEmail, pushNotifData, userRef = '' } = req.query;
    const { client } = faunaClient(req);
    const webPush = (await import('web-push')).default;
    console.log({
        userEmail, userRef, pushNotifData
    })
    webPush.setVapidDetails(
        'mailto:mr.ala72@gmail.com',
        process.env.VAPID_KEY_PUBLIC,
        process.env.VAPID_KEY_PRIVATE
    );
    const userRefExtracted = userRef
        .replace('Ref(Collection("users"), "','')
        .replace('")','')
    const query = userEmail? 
        q.Match(q.Index("-email@pushsubs+pushSub"), userEmail)
        :q.Match(q.Index("-userRef@pushsubs+pushSub"), q.Ref(q.Collection('users'), userRefExtracted));
    return client.query(
        q.Paginate(q.Reverse(query), {
            size: 10,
        })
    ).then(async ({data: pushSubsArray }) => {
        try {
            const parsedPushSubsArray = pushSubsArray.map(([ts, pushString]) => JSON.parse(pushString))
            let numberOfSent = 0;
            for (const pushObject of parsedPushSubsArray) {
                await webPush.sendNotification(pushObject, pushNotifData).then(
                    _ => {
                        numberOfSent += 1;
                    },
                    _ => console.log({_})
                    )
            }
            if(numberOfSent === 0) {
                console.error(userEmail, ' nothing sent');
                res.status(200).send();
                return;
            }
            res.json({email: userEmail, numberOfClients: numberOfSent, pushData: pushNotifData});
        } catch (error) {
            res.status(400).json(error)
        }
    }).catch(err=> res.status(400).send(err))
});


app.get('/tag-images', tagImages);

// if('development' === ENVIRONMENT) {
//     app.get('/tag-images', async (req, res) => {    
//         const { ballotId = '321702398424777216', images = [] } = req.query;
        // const images = [
        //     'https://cdn.imgress.com/images/mbf598f3ad40db18dfd1a9eee75788fc2/xWzKM60kPlHOqJwCwcP.jpg',
        //     // 'https://cdn.imgress.com/images/m50a74f19066bac0cece22f6d8b309440/TGooN9auHPaYNWJO3SS.jpg'
        // ]
//         const mockRefined =[
//             {
//                 "image_id": "1",
//                 "tags": [
//                     "outdoors",
//                     "person",
//                     "park",
//                     "outdoor",
//                     "adult",
//                     "people",
//                     "male",
//                     "happiness",
//                     "outside",
//                     "forest",
//                     "man",
//                     "tree",
//                     "active",
//                     "happy",
//                     "walking"
//                 ]
//             },
//             {
//                 "image_id": "2",
//                 "tags": [
//                     "tattoo",
//                     "design",
//                     "decoration"
//                 ]
//             }
//         ];
//         const allTags = [
//             "outdoors",
//             "person",
//             "park",
//             "outdoor",
//             "adult",
//             "people",
//             "male",
//             "happiness",
//             "outside",
//             "forest",
//             "man",
//             "tree",
//             "active",
//             "happy",
//             "walking",
//             "tattoo",
//             "design",
//             "decoration"
//         ]
//         const { client } = faunaClient(req);
//         await client.query(
//             q.Update(q.Ref(q.Collection('ballots'), ballotId), {
//                 data: {
//                     images_tags: mockRefined,
//                     all_images_tags: allTags
//                 }
//             })
//         ).then(r=>{
//             res.send(r.data)
//         })
//         return;
//     })
// }


// app.get('/presigned', async function(req, res) {
//     const Minio = (await import("minio"));
//     const fileName = nanoid(14)+'.jpg';
//     const s3Client = new Minio.Client({
//         endPoint: process.env.S3_BUCKET_URL.replace(/https?:\/\//, ''),
//         port: parseInt(process.env.S3_BUCKET_PORT) || 80,
//         useSSL: false,
//         accessKey: process.env.S3_BUCKET_ACCESS_ID,
//         secretKey: process.env.S3_BUCKET_SECRET_KEY
//     });
//     // s3Client.presignedUrl('post', process.env.S3_BUCKET_NAME, resultId).then(r=>console.log(r))
//     return s3Client.presignedPutObject(process.env.S3_BUCKET_NAME, fileName).then(
//         url=> translator({ statusCode: 200, body: { fileName: fileName, url }}, res),
//         err=> translator({ statusCode: 400, body: err}, res),
//     )
// })


//? upload path is only in node 
app.post('/upload', upload.single(process.env.FRONTEND_IMAGE_FILED_IN_FORMDATA), async function(req, res) {
    const S3 = (await import("aws-sdk/clients/s3.js")).default;
    try {
        let image; try { //? cloudflare
            const fd = await req.request.formData();
            image = fd.get(process.env.FRONTEND_IMAGE_FILED_IN_FORMDATA)
        } catch (err) {
            //? node - multer
            image = req.file.buffer
        }
        const port = process.env.S3_BUCKET_PORT? ':'+process.env.S3_BUCKET_PORT : ''
        const s3Client = new S3({
            endpoint: process.env.S3_BUCKET_URL + port ,
            accessKeyId: process.env.S3_BUCKET_ACCESS_ID,
            s3ForcePathStyle: true,
            secretAccessKey: process.env.S3_BUCKET_SECRET_KEY
        });
        // const backupS3Client = new S3({
        //     endpoint: process.env.S3_BACKUP_BUCKET_URL,
        //     accessKeyId: process.env.S3_BACKUP_BUCKET_ACCESS_ID,
        //     s3ForcePathStyle: true,
        //     secretAccessKey: process.env.S3_BACKUP_BUCKET_SECRET_KEY
        // });
        return getGoogleUser(req).then( async googleUser => {
            const key = nanoid(12);
            const resultId = `m${md5(googleUser.email + 'salt')}/${key}`+ '.jpg'
            const params = {
                Body: image, 
                ACL: "public-read",
                CacheControl: 'max-age=31536000; public',
                ContentType: 'image/jpeg',
                Bucket: process.env.S3_BUCKET_NAME, 
                Key: resultId,
            };
            return Promise.all([
                s3Client.putObject(params).promise(),
                // backupS3Client.putObject(params).promise(),
            ]).then(_=> {
                //* put object was success
                return translator({statusCode: 200, body:{id: resultId}}, res)
            }).catch(err => {
                console.error(err)
                // putt object failed
                return translator({statusCode: 400, body: err.toString()}, res)
            })
        }, (error) => {
            console.error(error)
            return translator({statusCode: 401, body: error.toString()}, res)
        }).catch((error) => {
            console.error(error)
            return translator({statusCode: 400, body: error.toString()}, res)
        })
    } catch (error) {
        return translator({statusCode: 566, body: error.toString()}, res)
    }
})

app.listen(port, () => {
  console.log(`Imgress node server is listening at ${port}`)
})