import faunadb from "faunadb";
let q = faunadb.query;

addEventListener("fetch", (event) => {
  if (event.request.method === "OPTIONS") {
    // Handle CORS preflight requests
    event.respondWith(handleOptions(event.request));
  } else {
    event.respondWith(handleRequest(event.request));
  }
});

async function handleRequest(request) {
  const routes = require("./src/core")(faunaStuff, (response)=>translator(response, request));
  const { path, params, headers, fd } = await urlDecoder(request);
  const result = await routes[path]({
    headers,
    request, //? relevenat for /upload only
    query: params,
    body: fd,
  });
  return result;
}






//* cf specifi utils
export async function urlDecoder(request) {
  const _url = new URL(request.url);
  const path = _url.pathname.replace(/\/$/, "");
  let params = Object.create(null);
  for (const [key, value] of new URLSearchParams(_url.search).entries()) {
    params[key] = value;
  }
  let headers = Object.create(null);
  for (const [key, value] of request.headers.entries()) {
    headers[key] = value;
  }
  let _fd;
  let fd = Object.create(null);
  try {
    _fd = await request.formData()
    for (const [key, value] of _fd.entries()) {
      fd[key] = value;
    }
  } catch (error) {}
  return {
    path,
    params,
    headers,
    fd
  };
}

function faunaStuff() {
  const client = new faunadb.Client({
    secret: process.env.FAUNA_SECRET,
    queryTimeout: 2500,
    keepAlive: false,
    fetch: (url, params) => {
      const signal = params.signal;
      delete params.signal;
      const abortPromise = new Promise((resolve) => {
        if (signal) {
          signal.onabort = resolve;
        }
      });
      return Promise.race([abortPromise, self.fetch(url, params)]);
    },
  });
  return {
    q,
    client,
  };
}

function translator(response, cfRequest) {
  return new Response(JSON.stringify(response.body), {
    status: response.statusCode,
    headers: corsHeaders(cfRequest)
  });
}


function validOrigin(currentOrigin) {
  if (
    [
      "https://imgress.com",
      "https://imgress-front.vercel.app",
      "https://imgress.netlify.app",
      "https://imgress.ir",
      "http://localhost:3301"
    ].includes(currentOrigin)
  ) {
    return currentOrigin;
  } else {
    return "No origin header";
  }
}

function corsHeaders (request) {
  const {headers} = request;
  return {
    "content-type": "application/json",
    "Access-Control-Allow-Methods": "GET,HEAD,POST,OPTIONS",
    "Access-Control-Max-Age": "86400",
    "Access-Control-Allow-Origin": validOrigin(headers.get("Origin")),
    "Access-Control-Allow-Headers": request.headers.get(
      "Access-Control-Request-Headers"
    ),
  }
}

function handleOptions(request) {
  // const {headers} = request;
  return new Response(null, {
    headers: corsHeaders(request),
  });
}