const createApi = require("unsplash-js").createApi;
const nodeFetch = require("node-fetch");
const {
  uniqueNamesGenerator,
  adjectives,
  colors,
  starWars,
} = require("unique-names-generator");
const q = require("faunadb").query;
const {
  getGoogleUser,
  serverError,
  baseline,
  successResponse,
  getRandomArbitrary,
  noOpResponse,
  normalizedList,
  unAuthenticated,
} = require("./utils");
const { userRefByEmail, CountOfIndexByMatch, safeGet, GetMatchOfIndex, failSafeContain } = require('./resuable-db-logics');


function pushNotifGen ( pushFormattedData, targetUserEmail, targetUserRef ) {
  const pushAppendix = { 
    ...pushFormattedData,
    "icon": "https://imgress.com/assets/logo.png",
    // "url": "https://imgress.com/notification",
  }
  const _email = targetUserEmail? `userEmail=${targetUserEmail}`: '';
  const _userRef = targetUserRef? `userRef=${targetUserRef}`: ''
  return nodeFetch(`${
    process.env.DEPOLY_URL
  }/pushnotif?${_email || _userRef}&pushNotifData=${JSON.stringify(pushAppendix)}`
  )
};


const config = {
  dictionaries: [adjectives, colors, starWars],
  separator: "-",
  length: 3,
}; // red_big_donkey
const randomNamer = () => uniqueNamesGenerator(config).split(" ")[0];


const ballotsSourceIndex = {
  unvoted: (_, email) =>
    q.Join(
      q.Difference(
        q.Match(q.Index("-is_public@ballots"), true),
        q.Match(q.Index("user_email@votes+ballot"), email)
      ),
      q.Index("-owner@ballots")
    ),
  featured: (_, email) =>
    q.Difference(
      q.Match(q.Index("all_anonymousBallots")),
      q.Match(q.Index("user_email@votes+ballot"), email)
    ),
  all: () => q.Match(q.Index("-is_public@ballots"), true),
  friends: (userId, email) =>
    q.Join(
      q.Match(
        q.Index("-votcher@rels+votchee"),
        // q.Ref(q.Collection('users'), userId)
        userRefByEmail(email)
      ),
      q.Index("-owner@ballots")
    ),
  user: (userId) =>
    q.Join(
      q.Match(q.Index("-ref@users"), q.Ref(q.Collection("users"), userId)),
      q.Index("-owner@ballots")
    ),
  tag: (tagName) =>
    q.Match(q.Index("-tag@ballots+ref"), tagName)
};

module.exports = function (faunaClient, translator) {
  return {
    "/ballots": async function (req, res) {
      const { type = 'all', userId, after, before, matchTerm } = req.query;
      const { client } = faunaClient(req);
      return translator(await baseline(query, req, normalizedList), res);

      function query(googleUser) {
        return client.query(
          q.Paginate(
            q.Reverse(ballotsSourceIndex[type](userId || matchTerm, googleUser.email)),
            {
              size: 3,
              after: after ? q.Ref(q.Collection("ballots"), after) : undefined,
              before: before
                ? q.Ref(q.Collection("ballots"), before)
                : undefined,
            }
          )
        ).then(r=>{
          console.log({r})
          return r;
        })
      }
    },

    //! -----------------------------------------------
    //! -----------------------------------------------

    "/ballotsprefeed": async function (req, res) {
      const type = req.query.type || "all";
      const userId = req.query.userId;
      const { client } = faunaClient(req);
      return translator(await baseline(query, req), res);

      function query(googleUser) {
        return client.query(
          q.Paginate(
            q.Join(
              q.Join(
                q.Join(
                  ballotsSourceIndex[type](userId, googleUser.email),
                  q.Index("-owner@ballots")
                ),
                q.Index("-ref@ballots+owners")
              ),
              q.Index("-ref@users+name+picture+username")
            )
          )
        );
      }
    },

    //! -----------------------------------------------
    //! -----------------------------------------------

    "/ballot": async (req, res) => {
      const ballotId = req.query.id;
      if (!ballotId) return translator({ statusCode: 400, body: "Id invalid" }, res);
      let googleUser = {};
      try {
        googleUser = await getGoogleUser(req);
      } catch (error) {}
      const { client } = faunaClient(req);
      const result = await client
      .query(
          q.Let(
            {
              ballot: q.Select(
                "data",
                q.Get(q.Ref(q.Collection("ballots"), ballotId))
              ),
              isPublic: q.Select("is_public", q.Var("ballot")),
              ballotOwnerRef: q.Select("owner", q.Var("ballot")),
              ballotOwnerData: q.Select(
                "data",
                safeGet(q.Var("ballotOwnerRef"))
              ),
              userInitiatedRequestedMatch: googleUser.email
                ? q.Get(q.Match(q.Index("-email@users"), googleUser.email))
                : {},
              userInitiatedRequestedRef: googleUser.email
                ? q.Select("ref", q.Var("userInitiatedRequestedMatch"))
                : {},
              userIsVotchedBallotOwner: googleUser.email
                ? q.IsNonEmpty(
                    q.Match(q.Index("-votchee-votcher@rels"), [
                      q.Var("ballotOwnerRef"),
                      q.Var("userInitiatedRequestedRef"),
                    ])
                  )
                : false,
              ballotOwnerExists: q.Exists(q.Var("ballotOwnerRef")),
              totalVotes: q.Count(
                q.Match(q.Index("-ballot_id@votes"), ballotId)
              ),
              userVote: q.Match(q.Index("-user_email-ballot_id@votes+vote"), [
                googleUser.email,
                ballotId,
              ]),
              voteValue: googleUser.email
                ? q.If(
                    q.IsNonEmpty(q.Var("userVote")),
                    q.Select(["data", "vote"], q.Get(q.Var("userVote"))),
                    null
                  )
                : null,
            },
            q.If(
              q.Var("ballotOwnerExists"),
              q.If(
                q.Var("isPublic"),
        /* ok */ q.Merge(q.Var("ballot"), {
                  owner: q.Var("ballotOwnerData"),
                  vote: q.Var("voteValue"),
                  totalVotes: q.Var("totalVotes"),
                  isAuthorized: true,
                }),
       /* nok */ q.If(
                  q.Or(
                    q.Var("userIsVotchedBallotOwner"),
                    q.Equals(
                      q.Var("userInitiatedRequestedRef"),
                      q.Var("ballotOwnerRef")
                    )
                  ),
          /* ok */ q.Merge(q.Var("ballot"), {
                    owner: q.Var("ballotOwnerData"),
                    vote: q.Var("voteValue"),
                    totalVotes: q.Var("totalVotes"),
                    isAuthorized: true,
                  }),
        /* nok */ {
                    owner: q.Var("ballotOwnerData"),
                    currentUser: googleUser.email,
                    isAuthorized: false,
                  }
                )
              ),
              { ownerDeactived: true }
            )
          )
        )
        .then(successResponse, serverError);
      return translator(result, res);
    },

    //! -----------------------------------------------
    //! -----------------------------------------------

    "/ballotvotes": async function (req, res) {
      const { client } = faunaClient(req);
      const ballotId = req.query.ballotId;
      const result = await client
        .query(q.Paginate(q.Match(q.Index("-ballot_id@votes"), ballotId)))
        .then(successResponse, serverError);
      return translator(result, res);
    },

    //! -----------------------------------------------
    //! -----------------------------------------------

    "/ballotvotsedetail": async function (req, res) {
      const ballotId = req.query.ballotId;
      if (!ballotId)
        return translator({ statusCode: 400, body: `username missing` }, res);
      const { client } = faunaClient(req);
      return translator(await baseline(query, req), res);

      function query(googleUser) {
        return client
          .query(
            q.Let(
              {
                currentUserRef: userRefByEmail(googleUser.email),
                ballot: q.Get(q.Ref(q.Collection("ballots"), ballotId)),
                ballotOwnerRef: q.Select(["data", "owner"], q.Var("ballot")),
                ownerIsVotchedByCurrent: q.IsNonEmpty(
                  q.Match(q.Index("-votchee-votcher@rels"), [
                    q.Var("ballotOwnerRef"),
                    q.Var("currentUserRef"),
                  ])
                ),
              },
              q.If(
                q.Or(
                  q.Equals(q.Var("currentUserRef"), q.Var("ballotOwnerRef")),
                  q.Var("ownerIsVotchedByCurrent")
                ),
                q.Paginate(
                  q.Reverse(
                    q.Match(q.Index("-ballot_id@votes+vote+user__id+user__username+user__name+user__picture+ts"), ballotId)
                  )
                ),
                {
                  data: {
                    forbidden: true,
                  },
                }
              )
            )
          )
          .then((response) =>
            Array.isArray(response.data)
              ? response.data.map(
                  ([vote, id, username, name, picture, ts]) => ({
                    vote,
                    id,
                    username,
                    name,
                    picture,
                    ts,
                  })
                )
              : response.data
          );
      }
    },

    //! -----------------------------------------------
    //! -----------------------------------------------

    "/castvote": async function (req, res) {
      const body = req.body;
      const ballotId = body.ballot_id;
      const voteIamgeId = body.image_id;
      const { client } = faunaClient(req);
      return translator(await baseline(query, req), res);

      function query(googleUser) {
        return client.query(
          q.Let(
            {
              ballotRef: q.Ref(q.Collection("ballots"), ballotId),
              ballot: q.Get(q.Var("ballotRef")),
              user: q.Get(q.Match(q.Index("-email@users"), googleUser.email)),
            },
            q.Do(
              q.Create(q.Collection("notifs"), {
                data: {
                  owner: q.Select(["data", "owner"], q.Var("ballot")),
                  sourceUser: q.Select("ref", q.Var("user")),
                  type: "vote",
                  seen: false,
                },
              }),
              q.Create(q.Collection("votes"), {
                data: {
                  //! TODO replace with private user function
                  owner: q.Select("ref", q.Var("user")),
                  user: q.Select("data", q.Var("user")),
                  ballot_owner: q.Select(["data", "owner"], q.Var("ballot")),
                  tracking_info: (req.request && req.request.cf) || {},
                  ts: q.Now(),
                  user_email: googleUser.email,
                  ballot: q.Var("ballotRef"),
                  ballot_id: ballotId,
                  vote: voteIamgeId,
                },
              })
            )
          )
        ).then( voteCollectionResult => {
          pushNotifGen({
            "title": 'Someone had voted on your post',
            "tag": 'vote',
          }, null, voteCollectionResult.data.ballot_owner)
          return voteCollectionResult;
        })
      }
    },

    //! -----------------------------------------------
    //! -----------------------------------------------

    "/createballot": async function (req, res) {
      let images, tags;
      const body = req.body;
      try {
        images = JSON.parse(body.images);
      } catch (error) {
        return translator({statusCode: 400, body: error.toString()}, res)
      }
      try {
        tags = JSON.parse(body.tags)
      } catch (error) {
        tags = []
      };

      const imagesIdOnly = images.map((obj) => obj.id);
      const { client } = faunaClient(req);
      return translator(await baseline(query, req), res);
      function query(googleUser) {
        const characterName = randomNamer();
        return client.query(
          q.Let(
            {
              userRef: userRefByEmail(googleUser.email),
              ballotId: q.NewId(),
              ballotRef: q.Ref(q.Collection("ballots"), q.Var("ballotId")),
            },
            q.Do(
              q.Create(q.Collection("images"), {
                data: {
                  imageKey: imagesIdOnly,
                  owner: q.Var("userRef"),
                  type: "Create_ballot",
                  ballotRef: q.Var("ballotRef"),
                },
              }),
              q.Create(q.Var("ballotRef"), {
                data: {
                  id: q.Var("ballotId"),
                  owner: q.Var("userRef"),
                  owner_email: googleUser.email,
                  is_public: Boolean(body.is_public - 0),
                  access_level: body.is_public - 0,
                  name: characterName,
                  description: body.description,
                  tags: tags,
                  created_at: q.Now(),
                  ends_in: q.TimeAdd(q.Now(), body.duration - 0, "hours"),
                  images: images,
                },
              }),
              {
                id: q.Var("ballotId"),
                coin: getRandomArbitrary(80, 120).toFixed(2),
              }
            )
          )
        ).then(async result => {
          const ballotId = result.id;
          nodeFetch(`${process.env.DEPOLY_URL}/tag-images?ballotId=${ballotId}&images=${imagesIdOnly}`)
          .catch(err=>console.log(err));
          return result;
        })
      }
    },

    //! -----------------------------------------------
    //! -----------------------------------------------

    "/createuser": async (req, res) => {
      const body = req.body;
      const { client } = faunaClient(req);
      return translator(await baseline(query, req), res);

      function query(googleUser) {
        const email = googleUser.email;
        return client.query(
          q.Let(
            { id: q.NewId() },
            q.Do(
              q.Create(q.Ref(q.Collection("users"), q.Var("id")), {
                data: {
                  email,
                  id: q.Var("id"),
                  picture: body.picture,
                  username: body.username,
                  is_active: true,
                  name: body.name,
                },
              }),
              q.Create(q.Collection("credentials"), {
                data: {
                  email,
                  // password: body.password,
                  id: q.Var("id"),
                  userRef: q.Ref(q.Collection("users"), q.Var("id")),
                },
              })
            )
          )
        );
      }
    },

    //! -----------------------------------------------
    //! -----------------------------------------------

    "/deleteballot": async function (req, res) {
      const ballotId = req.query.ballotId;
      if (!ballotId)
        return translator({ statusCode: 400, body: `ballotId missing` }, res);
      const { client } = faunaClient(req);
      return translator(await baseline(query, req), res);

      function query(googleUser) {
        return client.query(
          q.Let(
            {
              // currentUserMatch: q.Get(q.Match(q.Index("-email@users"), googleUser.email)),
              currentUserRef: userRefByEmail(googleUser.email),
              ballotRef: q.Ref(q.Collection("ballots"), ballotId),
              ballot: q.Get(q.Var("ballotRef")),
              ballotOwnerRef: q.Select(["data", "owner"], q.Var("ballot")),
            },
            q.If(
              q.Equals(q.Var("currentUserRef"), q.Var("ballotOwnerRef")),
              q.Do(
                q.Delete(q.Ref(q.Collection("ballots"), ballotId)),
                q.Create(q.Collection("deletedStuff"), {
                  data: {
                    type: "ballot",
                    id: ballotId,
                  },
                })
              ),
              {
                forbidden: true,
                data: noOpResponse("Unauthorized"),
              }
            )
          )
        );
      }
    },

    //! -----------------------------------------------
    //! -----------------------------------------------

    "/edituser": async (req, res) => {
      const body = req.body;
      const { client } = faunaClient(req);
      return translator(await baseline(query, req), res);

      function query(googleUser) {
        return client.query(
          q.Let(
            { userRef: userRefByEmail(googleUser.email) },
            q.Update(q.Var("userRef"), {
              data: {
                picture: body.picture || undefined,
                username: body.username || undefined,
                name: body.name || undefined,
                description: body.description || undefined,
              },
            })
          )
        );
      }
    },

    //! -----------------------------------------------
    //! -----------------------------------------------

    "/notifcount": async function (req, res) {
      const { client } = faunaClient(req);
      return translator(await baseline(query, req), res);

      function query(googleUser) {
        return client
          .query(
            q.Count(
              q.Union(
                q.Match(q.Index("-owner-seen@notifs"), [
                  userRefByEmail(googleUser.email),
                  false,
                ]),
                q.Match(q.Index("-target-seen@votchRequests"), [
                  userRefByEmail(googleUser.email),
                  false,
                ])
              )
            )
          )
          .then((count) => ({ data: { count } }));
      }
    },

    //! -----------------------------------------------
    //! -----------------------------------------------

    "/notifseenack": async function (req, res) {
      const { client } = faunaClient(req);
      return translator(await baseline(query, req), res);

      function query(googleUser) {
        return client.query(
          q.Foreach(
            q.Paginate(
              q.Union(
                q.Match(q.Index("-owner-seen@notifs"), [
                  userRefByEmail(googleUser.email),
                  false,
                ]),
                q.Match(q.Index("-target-seen@votchRequests"), [
                  userRefByEmail(googleUser.email),
                  false,
                ])
              )
            ),
            q.Lambda(
              "notifDocRef",
              q.Update(q.Var("notifDocRef"), {
                data: {
                  seen: true,
                },
              })
            )
          )
        );
      }
    },

    //! -----------------------------------------------
    //! -----------------------------------------------

    "/notiflist": async function (req, res) {
      const { client } = faunaClient(req);
      return translator(await baseline(query, req), res);

      function query(googleUser) {
        return client.query(
          q.Let(
            {
              currentUserRef: userRefByEmail(googleUser.email),
            },
            q.Filter(
              q.Map(
                  q.Paginate(
                    q.Reverse(q.Match(q.Index("-owner@notifs"), q.Var("currentUserRef"))),
                  {size: 30}
                ),
                q.Lambda(
                  "notifRef",
                  q.Let(
                    {
                      notifMatch: safeGet(q.Var("notifRef")),
                      sourceUserRef: q.Select(
                        ["data", "sourceUser"],
                        q.Var("notifMatch")
                      ),
                    },
                    q.If(
                      q.Exists(q.Var("sourceUserRef")),
                      {
                        type: q.Select(["data", "type"], q.Var("notifMatch")),
                        user: q.Select("data", q.Get(q.Var("sourceUserRef"))),
                        seen: q.Select(["data", "seen"], q.Var("notifMatch")),
                      },
                      null
                    )
                  )
                )
              ),
              q.Lambda("notifObject", q.IsObject(q.Var("notifObject")))
            )
          )
        );
      }
    },

    //! -----------------------------------------------
    //! -----------------------------------------------

    "/undovotchrequest": async function (req, res) {
      const id = req.query.id;
      if (!id)
        return translator({ statusCode: 400, body: `username missing` }, res);

      const { client } = faunaClient(req);
      return translator(await baseline(query, req), res);

      function query(googleUser) {
        return client.query(
          q.Let(
            {
              currentUserRef: userRefByEmail(googleUser.email),
              targetUserMatch: q.Get(q.Match(q.Index("-id@users+id+username+name+picture"), id)),
              targetUserRef: q.Select("ref", q.Var("targetUserMatch")),
              votchRequestRef: q.Match(
                q.Index("-user-target@votchRequests"),
                [
                  //? request man ke targetesh fellani e
                  q.Var("currentUserRef"), //? user
                  q.Var("targetUserRef"), //? target
                ]
              ),
            },
            q.If(
              q.Or(
                q.Equals(q.Var("currentUserRef"), q.Var("targetUserRef")),
                q.IsEmpty(q.Var("votchRequestRef"))
              ),
              {
                data: noOpResponse(),
              },
              q.Delete(q.Select("ref", q.Get(q.Var("votchRequestRef"))))
            )
          )
        );
      }
    },

    //! -----------------------------------------------
    //! -----------------------------------------------

    "/unvotch": async function (req, res) {
      const id = req.query.id;
      if (!id)
        return translator({ statusCode: 400, body: `username missing` }, res);
      const { client } = faunaClient(req);
      return translator(await baseline(query, req), res);

      function query(googleUser) {
        return client.query(
          q.Let(
            {
              currentUserRef: userRefByEmail(googleUser.email),
              targetUserRef: q.Select(
                "ref",
                q.Get(q.Match(q.Index("-id@users+id+username+name+picture"), id))
              ),
            },
            q.If(
              q.Not(q.Equals(q.Var("currentUserRef"), q.Var("targetUserRef"))),
              q.Delete(
                q.Select(
                  "ref",
                  q.Get(
                    q.Match(q.Index("-votchee-votcher@rels"), [
                      q.Var("targetUserRef"),
                      q.Var("currentUserRef"),
                    ])
                  )
                )
              ),
              noOpResponse()
            )
          )
        );
      }
    },

    //! -----------------------------------------------
    //! -----------------------------------------------

    "/updatevote": async function (req, res) {
      const body = req.body;
      const ballotId = body.ballot_id;
      const voteIamgeId = body.image_id;
      // const view_duration = body.view_duration;
      const { client } = faunaClient(req);
      return translator(await baseline(query, req), res);

      function query(googleUser) {
        return client.query(
          q.Update(
            q.Select(
              "ref",
              q.Get(
                q.Match(q.Index("-user_email-ballot_id@votes+vote"), [
                  googleUser.email,
                  ballotId,
                ])
              )
            ),
            {
              data: {
                vote: voteIamgeId,
                ts: q.Now(),
              },
            }
          )
        );
      }
    },

    //! -----------------------------------------------
    //! -----------------------------------------------

    "/userballots": async function (req, res) {
      const id = req.query.id;
      const username = req.query.username;
      if (!(id || username))
        return translator({ statusCode: 400, body: `username missing` }, res);
      const { client } = faunaClient(req);
      return translator(await baseline(query, req), res);

      function query(googleUser) {
        return client.query(
          q.Let(
            {
              targetUserMatch: username
                ? q.Get(q.Match(q.Index("-username@users"), username))
                : q.Get(q.Match(q.Index("-id@users+id+username+name+picture"), id)),
              targetUserRef: q.Select("ref", q.Var("targetUserMatch")),
            },
            q.Map(
              q.Reverse(
                q.Paginate(
                  q.Match(q.Index("-owner-is_public@ballots"), [
                    q.Var("targetUserRef"),
                    true,
                  ])
                )
              ),
              q.Lambda("ballotRef", q.Select("data", q.Get(q.Var("ballotRef"))))
            )
          )
        );
      }
    },

    //! -----------------------------------------------
    //! -----------------------------------------------

    "/userlist": async (req, res) => {
      const id = req.query.id;
      const username = req.query.username;
      const { client } = faunaClient(req);
      return translator(await baseline(query, req), res);

      function query(googleUser) {
        return client.query(
          q.Let(
            {
              targetUser: id
                ? q.Get(q.Match(q.Index("-id@users+id+username+name+picture"), id))
                : q.Get(q.Match(q.Index("-username@users"), username)),
              targetUserRef: q.Select("ref", q.Var("targetUser")),
              currentUserRef: userRefByEmail(googleUser.email),
              totalVotes: CountOfIndexByMatch(
                "-owner@votes",
                q.Var("targetUserRef"),
              ),
              votchers: CountOfIndexByMatch(
                "-votchee@rels+votcher",
                q.Var("targetUserRef"),
              ),
              votchees: CountOfIndexByMatch(
                "-votcher@rels+votchee",
                q.Var("targetUserRef"),
              ),
            },
            q.Merge(q.Select("data", q.Var("targetUser")), {
              is_votched: q.IsNonEmpty(
                q.Match(q.Index("-votchee-votcher@rels"), [
                  q.Var("targetUserRef"),
                  q.Var("currentUserRef"),
                ])
              ),
              pending_votch: q.IsNonEmpty(
                q.Match(q.Index("-user-target@votchRequests"), [
                  q.Var("currentUserRef"),
                  q.Var("targetUserRef"),
                ])
              ),
              total_votes: q.Var("totalVotes"),
              votchers: q.Var("votchers"),
              votchees: q.Var("votchees"),
            })
          )
        );
      }
    },

    //! -----------------------------------------------
    //! -----------------------------------------------

    "/usernamecheck": async (req, res) => {
      const username = req.query.username;
      if (!username)
        return translator({ statusCode: 400, body: `username missing` }, res);
      if (username.length < 4)
        return translator({ statusCode: 400, body: `username too short` }, res);
      const { client } = faunaClient(req);
      const result = await client
        .query({
          isAvalaible: q.IsEmpty(q.Match(q.Index("-username@users+username"), username)),
        })
        .then(successResponse, serverError);
      return translator(result, res);
    },

    //! -----------------------------------------------
    //! -----------------------------------------------

    "/userprivateballots": async function (req, res) {
      const id = req.query.id;
      const username = req.query.username;
      if (!(id || username))
        return translator({ statusCode: 400, body: `username missing` }, res);
      const { client } = faunaClient(req);
      return translator(await baseline(query, req), res);

      function query(googleUser) {
        return client.query(
          q.Let(
            {
              currentUserRef: userRefByEmail(googleUser.email),
              targetUserMatch: username
                ? q.Get(q.Match(q.Index("-username@users"), username))
                : q.Get(q.Match(q.Index("-id@users+id+username+name+picture"), id)),
              targetUserRef: q.Select("ref", q.Var("targetUserMatch")),
              targetIsVotchedByCurrent: q.IsNonEmpty(
                q.Match(q.Index("-votchee-votcher@rels"), [
                  q.Var("targetUserRef"),
                  q.Var("currentUserRef"),
                ])
              ),
            },
            q.If(
              q.Or(
                q.Equals(q.Var("currentUserRef"), q.Var("targetUserRef")),
                q.Var("targetIsVotchedByCurrent")
              ),
              q.Map(
                q.Reverse(
                  q.Paginate(
                    q.Match(q.Index("-owner-is_public@ballots"), [
                      q.Var("targetUserRef"),
                      false,
                    ])
                  )
                ),
                q.Lambda(
                  "ballotRef",
                  q.Select("data", q.Get(q.Var("ballotRef")))
                )
              ),
              {
                data: {
                  forbidden: true,
                },
              }
            )
          )
        );
      }
    },
    
    //! -----------------------------------------------
    //! -----------------------------------------------

    "/users": async (req, res) => {
      const { client } = faunaClient(req);
      return translator(await baseline(query, req), res);

      function query(googleUser) {
        return client.query(
          q.Get(q.Match(q.Index("-email@users"), googleUser.email))
        );
      }
    },

    //! -----------------------------------------------
    //! -----------------------------------------------

    "/votchconfirm": async function (req, res) {
      const id = req.query.id;
      if (!id)
        return translator({ statusCode: 400, body: `username missing` }, res);
      const { client } = faunaClient(req);
      return translator(await baseline(query, req), res);

      function query(googleUser) {
        return client.query(
          q.Let(
            {
              // currentUserMatch: q.Get(q.Match(q.Index("-email@users"), googleUser.email)),
              currentUserRef: userRefByEmail(googleUser.email),
              targetUserMatch: q.Get(q.Match(q.Index("-id@users+id+username+name+picture"), id)),
              targetUserRef: q.Select("ref", q.Var("targetUserMatch")),
              votchRequestRef: q.Match(
                q.Index("-user-target@votchRequests"),
                [
                  //? request fellani ke targetesh manam
                  q.Var("targetUserRef"), //? user
                  q.Var("currentUserRef"), //? target
                ]
              ),
            },
            q.If(
              q.Or(
                q.Equals(q.Var("currentUserRef"), q.Var("targetUserRef")),
                q.IsEmpty(q.Var("votchRequestRef"))
              ),
              { data: noOpResponse("no votch request") },
              q.Do(
                q.Delete(q.Select("ref", q.Get(q.Var("votchRequestRef")))),
                q.Create(q.Collection("rels"), {
                  data: {
                    votchee: q.Var("currentUserRef"),
                    votcher: q.Var("targetUserRef"),
                  },
                }),
                q.Create(q.Collection("notifs"), {
                  data: {
                    owner: q.Var("targetUserRef"),
                    sourceUser: q.Var("currentUserRef"),
                    type: "follow-accepted",
                    seen: false,
                  },
                })
              )
            )
          )
        ).then( doc => {
          pushNotifGen({
            "title": 'Accepted follow request',
            "tag": 'follow',
          }, null, doc.data.owner);
          return doc;
        })
      }
    },

    //! -----------------------------------------------
    //! -----------------------------------------------

    "/search": async function (req, res) {
      const { client } = faunaClient(req);
      const searchTerm = req.query.searchTerm;
      if (!searchTerm) serverError("no searchterm");
      return translator(await baseline(query, req), res);
      function query(googleUser) {
        return client.query(
            q.Let(
              {
                currentUserRef: userRefByEmail(googleUser.email),
              },
              q.Map(
                q.Filter(
                  q.Paginate(q.Reverse(q.Match(q.Index("@users+username+name+id+picture"))), {
                    size: 20
                  }),
                  q.Lambda(
                    ["ref", "username", "name", "id", "picture"],
                    q.Or(
                      failSafeContain(q.Var("username"), searchTerm),
                      failSafeContain(q.Var("name"), searchTerm),
                    )
                  )
                ),
          //       {
          //          size: 10
          //        }
          //     ),
                q.Lambda(["user", "username", "name", "id", "picture"], {
                  username: q.Var("username"),
                  name: q.Var("name"),
                  picture: q.Var("picture"),
                  id: q.Var("id")
                })
              )
            )
          );
      }
    },

    //! -----------------------------------------------
    //! -----------------------------------------------
    "/votcheesfeed": async function (req, res) {
      const { client } = faunaClient(req);
      return translator(await baseline(query, req, normalizedList), res);

      function query(googleUser) {
        return client.query(
          q.Let(
            {
              // currentUserMatch: q.Get(q.Match(q.Index("-email@users"), googleUser.email)),
              currentUserRef: userRefByEmail(googleUser.email),
            },
            q.Paginate(
              q.Reverse(
                q.Join(
                  q.Match(
                    q.Index("-votcher@rels+votchee"),
                    q.Var("currentUserRef")
                  ),
                  q.Index("-owner@ballots")
                )
              )
              // q.Lambda('ballot', q.Select(['data'], q.Get(q.Var('ballot'))))
            )
          )
        );
      }
    },

    //! -----------------------------------------------
    //! -----------------------------------------------

    "/votcheeslist": async function (req, res) {
      const id = req.query.id;
      if (!id)
        return translator({ statusCode: 400, body: `username missing` }, res);
      const { client } = faunaClient(req);
      return translator(await baseline(query, req), res);

      function query(googleUser) {
        return client
          .query(
            q.Let(
              {
                // currentUserMatch: q.Get(q.Match(q.Index("-email@users"), googleUser.email)),
                currentUserRef: userRefByEmail(googleUser.email),
                targetUserMatch: q.Get(q.Match(q.Index("-id@users+id+username+name+picture"), id)),
                targetUserRef: q.Select("ref", q.Var("targetUserMatch")),
                targetIsVotchedByCurrent: q.IsNonEmpty(
                  q.Match(q.Index("-votchee-votcher@rels"), [
                    q.Var("targetUserRef"),
                    q.Var("currentUserRef"),
                  ])
                ),
              },
              q.If(
                q.Or(
                  q.Equals(q.Var("currentUserRef"), q.Var("targetUserRef")),
                  q.Var("targetIsVotchedByCurrent")
                ),
                q.Map(
                  q.Paginate(
                    q.Match(
                      q.Index("-votcher@rels+votchee"),
                      q.Var("targetUserRef")
                    )
                  ),
                  q.Lambda(
                    "userRef",
                    q.Select("data", safeGet(q.Var("userRef")))
                  )
                ),
                {
                  data: {
                    forbidden: true,
                  },
                }
              )
            )
          )
          .then((response) =>
            Array.isArray(response.data)
              ? response.data.filter((_) => _)
              : response.data
          );
      }
    },

    //! -----------------------------------------------
    //! -----------------------------------------------

    "/votcherslist": async function (req, res) {
      const id = req.query.id;
      if (!id)
        return translator({ statusCode: 400, body: `username missing` }, res);
      const { client } = faunaClient(req);
      return translator(await baseline(query, req), res);

      function query(googleUser) {
        return client
          .query(
            q.Let(
              {
                // currentUserMatch: q.Get(q.Match(q.Index("-email@users"), googleUser.email)),
                currentUserRef: userRefByEmail(googleUser.email),
                targetUserMatch: q.Get(q.Match(q.Index("-id@users+id+username+name+picture"), id)),
                targetUserRef: q.Select("ref", q.Var("targetUserMatch")),
                targetIsVotchedByCurrent: q.IsNonEmpty(
                  q.Match(q.Index("-votchee-votcher@rels"), [
                    q.Var("targetUserRef"),
                    q.Var("currentUserRef"),
                  ])
                ),
              },
              q.If(
                q.Or(
                  q.Equals(q.Var("currentUserRef"), q.Var("targetUserRef")),
                  q.Var("targetIsVotchedByCurrent")
                ),
                q.Map(
                  q.Reverse(
                    q.Paginate(
                      q.Match(
                        q.Index("-votchee@rels+votcher"),
                        q.Var("targetUserRef")
                      )
                    )
                  ),
                  q.Lambda(
                    "userRef",
                    q.Select("data", safeGet(q.Var("userRef")))
                  )
                ),
                {
                  data: {
                    forbidden: true,
                  },
                }
              )
            )
          )
          .then((response) =>
            Array.isArray(response.data)
              ? response.data.filter((_) => _)
              : response.data
          );
      }
    },

    //! -----------------------------------------------
    //! -----------------------------------------------

    "/votchignore": async function (req, res) {
      const id = req.query.id;
      if (!id)
        return translator({ statusCode: 400, body: `username missing` }, res);
      const { client } = faunaClient(req);
      return translator(await baseline(query, req), res);

      function query(googleUser) {
        return client.query(
          q.Let(
            {
              // currentUserMatch: q.Get(q.Match(q.Index("-email@users"), googleUser.email)),
              currentUserRef: userRefByEmail(googleUser.email),
              targetUserMatch: q.Get(q.Match(q.Index("-id@users+id+username+name+picture"), id)),
              targetUserRef: q.Select("ref", q.Var("targetUserMatch")),
              votchRequestRef: q.Match(
                q.Index("-user-target@votchRequests"),
                [
                  //? request fellani ke targetesh manam
                  q.Var("targetUserRef"), //? user
                  q.Var("currentUserRef"), //? target
                ]
              ),
            },
            q.If(
              q.Or(
                q.Equals(q.Var("currentUserRef"), q.Var("targetUserRef")),
                q.IsEmpty(q.Var("votchRequestRef"))
              ),
              {
                data: noOpResponse(),
              },
              q.Delete(q.Select("ref", q.Get(q.Var("votchRequestRef"))))
            )
          )
        );
      }
    },

    //! -----------------------------------------------
    //! -----------------------------------------------

    "/votchrequest": async function (req, res) {
      const id = req.query.id;
      if (!id)
        return translator({ statusCode: 400, body: `username missing` }, res);
      const { client } = faunaClient(req);
      return translator(await baseline(query, req), res);

      function query(googleUser) {
        return client.query(
          q.Let(
            {
              // currentUserMatch: q.Get(q.Match(q.Index("-email@users"), googleUser.email)),
              currentUserRef: userRefByEmail(googleUser.email),
              targetUserMatch: q.Get(q.Match(q.Index("-id@users+id+username+name+picture"), id)),
              targetUserRef: q.Select("ref", q.Var("targetUserMatch")),
            },
            q.If(
              q.Or(
                q.Equals(q.Var("currentUserRef"), q.Var("targetUserRef")),
                q.IsNonEmpty(
                  q.Match(q.Index("-user-target@votchRequests"), [
                    //! validtae the order here
                    q.Var("currentUserRef"),
                    q.Var("targetUserRef"),
                  ])
                ),
                q.IsNonEmpty(
                  q.Match(q.Index("-votchee-votcher@rels"), [
                    //! validtae the order here
                    q.Var("targetUserRef"),
                    q.Var("currentUserRef"),
                  ])
                )
              ),
              {
                data: noOpResponse("Already submitted"),
              },
              q.Create(q.Collection("votchRequests"), {
                data: {
                  seen: false,
                  user: q.Var("currentUserRef"),
                  target: q.Var("targetUserRef"),
                },
              })
            )
          )
        ).then( doc => {
          pushNotifGen({
            "title": 'You have new follow request',
            "tag": 'follow',
          }, null, doc.data.target);
          return doc;
        })
      }
    },

    //! -----------------------------------------------
    //! -----------------------------------------------

    "/votchrequestlist": async function (req, res) {
      const { client } = faunaClient(req);
      return translator(await baseline(query, req), res);

      function query(googleUser) {
        return client
          .query(
            q.Let(
              {
                // currentUserMatch: q.Get(q.Match(q.Index("-email@users"), googleUser.email)),
                currentUserRef: userRefByEmail(googleUser.email),
                usersOfVotchRequests: q.Match(
                  q.Index("-target@votchRequests+user"),
                  q.Var("currentUserRef")
                ),
              },
              q.Map(
                q.Reverse(q.Paginate(q.Var("usersOfVotchRequests"))),
                q.Lambda(
                  ["userRef"],
                  q.Select("data", safeGet(q.Var("userRef")))
                )
              )
            )
          )
          .then(function (response) {
            if ( response.data.flat ) {
              return response.data.flat(2).filter(O=>O);
            } else {
              return [];
            }
          });
      }
    },

    //! -----------------------------------------------
    //! -----------------------------------------------

    "/imgfeed": async (req, res) => {
      const searchTerm = req.query.searchTerm;
      const unsplash = createApi({
        accessKey: "qeMyYOeQfmPDki6TPsDPZtFicDVi4aqnBMlvrYRCsqU",
        fetch: nodeFetch,
      });
      return translator(await baseline(query, req), res);
      async function query(googleUser) {
        return unsplash.search.getPhotos({
          query: searchTerm,
          page: 1,
          perPage: 50,
          // color: 'green',
          // orientation: 'portrait',
        });
      }
    },

    //! -----------------------------------------------
    //! -----------------------------------------------

    "/anonymousballotsupdatevote": async (req, res) => {
      const ballotId = req.body.ballot_id;
      const voteIamgeId = req.body.image_id;
      const { client } = faunaClient(req);
      return translator(await baseline(query, req), res);
      function query(googleUser) {
        return client.query(
          q.Update(
            q.Select(
              "ref",
              q.Get(
                q.Match(q.Index("-user_email-ballot_id@votes+vote"), [
                  googleUser.email,
                  ballotId,
                ])
              )
            ),
            {
              data: {
                tracking_info: (req.request && req.request.cf) || {},
                ts: q.Now(),
                vote: voteIamgeId,
              },
            }
          )
        );
      }
    },
    "/anonymousballotsvote": async (req, res) => {
      const ballotId = req.body.ballot_id;
      const voteIamgeId = req.body.image_id;
      const { client } = faunaClient(req);
      return translator(await baseline(query, req), res);
      function query(googleUser) {
        return client.query(
          q.Create(q.Collection("votes"), {
            data: {
              type: "anonymous",
              owner: userRefByEmail(googleUser.email),
              tracking_info: (req.request && req.request.cf) || {},
              ts: q.Now(),
              user_email: googleUser.email,
              ballot: q.Ref(q.Collection("ballots"), ballotId),
              ballot_id: ballotId,
              vote: voteIamgeId,
            },
          })
        );
      }
    },
    //! -----------------------------------------------
    //! -----------------------------------------------

    "/anonymousballots": async (req, res) => {
      const { client } = faunaClient(req);
      return translator(await baseline(query, req), res);
      function query(googleUser) {
        return client.query(
          q.Map(
            q.Paginate(q.Match(q.Index("all_anonymousBallots"))),
            q.Lambda("ref", q.Get(q.Var("ref")))
          )
        );
      }
    },
    //! -----------------------------------------------
    //! -----------------------------------------------

    "/createanonymous": async (req, res) => {
      const body = req.body;
      const images = JSON.parse(body.images);
      const tags = JSON.parse(body.tags);
      if (!Array.isArray(tags)) {
        throw "err";
      }
      const _images = images.map((obj) => ({
        id: obj.id,
        url: obj.url.replace(
          "images.unsplash.com",
          "calm-mouse-8a17.morezla73.workers.dev"
        ),
        unsplashUserId: obj.userId,
        thumb: obj.thumb,
        tags: obj.tags,
        alt: obj.alt,
        unsplashUserName: obj.userName,
      }));
      const characterName = randomNamer();
      const { client } = faunaClient(req);
      return translator(await baseline(query, req), res);
      function query(googleUser) {
        return client.query(
          q.Let(
            {
              userRef: userRefByEmail(googleUser.email),
              anonymousBallotId: q.NewId(),
              anonymousBallotRef: q.Ref(
                q.Collection("anonymousBallots"),
                q.Var("anonymousBallotId")
              ),
            },
            q.Do(
              q.Create(q.Var("anonymousBallotRef"), {
                data: {
                  id: q.Var("anonymousBallotId"),
                  owner: q.Var("userRef"),
                  name: characterName,
                  created_at: q.Now(),
                  type: "anonymous",
                  ballot: q.Var("anonymousBallotRef"),
                  images: _images,
                  tags,
                },
              }),
              {
                id: q.Var("anonymousBallotId"),
              }
            )
          )
        );
      }
    },
    //! -----------------------------------------------
    //! -----------------------------------------------

    "/anonymousballot": async (req, res) => {
      const { client } = faunaClient(req);
      const ballotId = req.query.id;
      return translator(await baseline(query, req), res);
      function query(googleUser) {
        return client.query(
          q.Let(
            {
              userVote: q.Match(q.Index("-user_email-ballot_id@votes+vote"), [
                googleUser.email,
                ballotId,
              ]),
            },
            q.Merge(
              q.Select(
                "data",
                q.Get(q.Ref(q.Collection("anonymousBallots"), ballotId))
              ),
              {
                totalVotes: q.Count(
                  q.Match(q.Index("-ballot_id@votes"), ballotId)
                ),
                vote: q.Select(
                  ["data", "vote"],
                  safeGet(q.Var("userVote")),
                  null
                ),
              }
            )
          )
        );
      }
    },
    //! -----------------------------------------------
    //! -----------------------------------------------

    "/savepush": async (req, res) => {
      const { pushSubString, deviceId } = req.body;
      const { client } = faunaClient(req);
      return translator(await baseline(query, req), res);
      function query(googleUser) {
        return client.query(
          q.Create(q.Collection("pushsubs"), {
            data: {
              deviceId,
              userRef: userRefByEmail(googleUser.email),
              userEmail: googleUser.email,
              pushSub : pushSubString,
              lastTime: q.Now()
            }
          }),
        );
      }
    },
    //! -----------------------------------------------
    //! -----------------------------------------------

    "/tags": async (req, res) => {
      const { client } = faunaClient(req);
      const result = await client.query(
        q.Paginate(q.Match(q.Index('@ballots+all_images_tags')))
      ).then(successResponse, serverError);
      return translator(result, res);
    },
    //! -----------------------------------------------
    //! -----------------------------------------------

    "/ballots-by-tags": async (req, res) => {
      let tags;
      const _tags = req.body.tags;
      try {
        tags = JSON.parse(_tags);
      } catch (error) {
        if (Array.isArray(_tags)) {
          tags = _tags;
        }
        throw Error('no tags')
      }
      const { client } = faunaClient(req);
      const result = await client.query(
        q.Map(
          q.Paginate(q.Match(q.Index('-tag@ballots+ref'), tags)),
          q.Lambda('ballotRef', q.Get(q.Var('ballotRef')))
        )
      ).then(successResponse, serverError);
      return translator(result, res);
    },
    //! -----------------------------------------------
    //! -----------------------------------------------

    "/now": async (req, res) => {
      const { client } = faunaClient(req);
      const result = await client.query(q.Now()).then(successResponse, serverError);
      return translator(result, res);
    },
    //? -----------------------------------------------
    //? -----------------------------------------------
  };
};
