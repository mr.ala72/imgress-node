const faunadb = require('faunadb');
const collections = require('./collections');
const indexes = require('./indexes');


const envs = [
  './.env.development',
  './.env.staging',
  './.env.production'
];
require('dotenv').config({
  path: envs[0]
});

if(!process.env.FAUNA_SECRET) throw Error('fauna not initilized');

const client = new faunadb.Client({ 
  secret: process.env.FAUNA_SECRET,
  domain: process.env.FAUNA_DOMAIN,
  port: process.env.FAUNA_PORT,
  scheme: process.env.FAUNA_SCHEME,
});


//? uncomment the one you need 
// collections(client)
// indexes(client)
collections(client).then(()=>indexes(client))

.then(()=>process.exit(0))

