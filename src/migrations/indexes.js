const q = require('faunadb').query;

async function fireTheEngine ( faunaClient ) {
  for (const indxObj of indexes) {
    await faunaClient.query(
      indxObj.fullQuery?
      q.CreateIndex(indxObj)
      :q.CreateIndex({
        name: indxObj.name,
        source: q.Collection(indxObj.source),
        terms: indxObj.terms,
        values: indxObj.values,
        serialized: indxObj.serialized,
        unique: indxObj.unique,
      })
    ).then(
      () => console.log(indxObj.name + ' insterted'),
      (err) => console.log(err.toString())
    )
  }
}

const indexes = [
  {
    active: true,
    serialized: true,
    // name_old: "users_by_email",
    name: "-email@users",
    permissions: {
      read: "public"
    },
    source: "users",
    terms: [
      {
        field: ["data", "email"]
      }
    ],
    unique: true,
    partitions: 1
  },
  {
    active: true,
    serialized: true,
    // name_old: "users_by_username",
    name: "-username@users",
    permissions: {
      read: "public"
    },
    source: "users",
    terms: [
      {
        field: ["data", "username"]
      }
    ],
    unique: true,
    partitions: 1
  },
  {
    active: true,
    serialized: true,
    // name_old: "public_ballots",
    name: "-is_public@ballots",
    source: "ballots",
    terms: [
      {
        field: ["data", "is_public"]
      }
    ],
    unique: false,
    partitions: 1
  },
  {
    active: true,
    serialized: true,
    // name_old: "votes_by_ballot",
    name: "-ballot@votes",
    unique: false,
    source: "votes",
    terms: [
      {
        field: ["data", "ballot"]
      }
    ],
    partitions: 1
  },
  {
    active: true,
    serialized: true,
    // name_old: "votes_by_user",
    name: "-user@votes",
    unique: false,
    source: "votes",
    terms: [
      {
        field: ["data", "user"]
      }
    ],
    partitions: 1
  },
  {
    active: true,
    serialized: true,
    // name_old: "votes_by_ballot_id",
    name: "-ballot_id@votes",
    unique: false,
    source: "votes",
    terms: [
      {
        field: ["data", "ballot_id"]
      }
    ],
    values: [
      {
        field: ["data", "vote"]
      }
    ],
    partitions: 1
  },
  {
    active: true,
    serialized: true,
    // name_old: "vote_by_email_and_ballot_id",
    name: "-user_email-ballot_id@votes+vote",
    unique: true,
    source: "votes",
    terms: [
      {
        field: ["data", "user_email"]
      },
      {
        field: ["data", "ballot_id"]
      }
    ],
    values: [
      {
        field: ["data", "vote"]
      }
    ],
    partitions: 1
  },
  {
    active: true,
    serialized: true,
    // name_old: "coins_by_user",
    name: "-user@coins",
    unique: false,
    source: "coins",
    terms: [
      {
        field: ["data", "user"]
      }
    ],
    partitions: 1
  },
  {
    active: true,
    serialized: true,
    // name_old: "users_fields_by_email",
    name: "-email@users+username+name+picture",
    unique: true,
    source: "users",
    terms: [
      {
        field: ["data", "email"]
      }
    ],
    values: [
      {
        field: ["data", "username"]
      },
      {
        field: ["data", "name"]
      },
      {
        field: ["data", "picture"]
      }
    ],
    partitions: 1
  },
  {
    active: true,
    serialized: true,
    // name_old: "is_votched_by",
    name: "-votchee-votcher@rels",
    unique: true,
    source: "rels",
    terms: [
      {
        field: ["data", "votchee"]
      },
      {
        field: ["data", "votcher"]
      }
    ],
    partitions: 1
  },
  {
    active: true,
    serialized: true,
    // name_old: "ballots_by_user",
    name: "-owner@ballots",
    unique: false,
    source: "ballots",
    terms: [
      {
        field: ["data", "owner"]
      }
    ],
    partitions: 1
  },
  {
    active: true,
    serialized: true,
    // name_old: "ballots_by_user_and_public",
    name: "-owner-is_public@ballots",
    unique: false,
    source: "ballots",
    terms: [
      {
        field: ["data", "owner"]
      },
      {
        field: ["data", "is_public"]
      }
    ],
    partitions: 1
  },
  {
    active: true,
    serialized: true,
    // name_old: "votchers_by_votchee",
    name: "-votchee@rels+votcher",
    unique: false,
    source: "rels",
    terms: [
      {
        field: ["data", "votchee"]
      }
    ],
    values: [
      {
        field: ["data", "votcher"]
      }
    ],
    partitions: 1
  },
  {
    active: true,
    serialized: true,
    // name_old: "ballots_voted_by_user",
    name: "-user-ballot@votes",
    unique: false,
    source: "votes",
    terms: [
      {
        field: ["data", "user"]
      }
    ],
    values: [
      {
        field: ["data", "ballot"]
      }
    ],
    partitions: 1
  },
  {
    active: true,
    serialized: true,
    // name_old: "ballots_voted_by_user_email",
    name: "user_email@votes+ballot",
    unique: false,
    source: "votes",
    terms: [
      {
        field: ["data", "user_email"]
      }
    ],
    values: [
      {
        field: ["data", "ballot"]
      }
    ],
    partitions: 1
  },
  {
    active: true,
    serialized: true,
    // name_old: "special_ballots",
    name: "-special@ballots",
    unique: false,
    source: "ballots",
    terms: [
      {
        field: ["data", "special"]
      }
    ],
    partitions: 1
  },
  {
    active: true,
    serialized: true,
    // name_old: "votchees_by_votcher",
    name: "-votcher@rels+votchee",
    unique: false,
    source: "rels",
    terms: [
      {
        field: ["data", "votcher"]
      }
    ],
    values: [
      {
        field: ["data", "votchee"]
      }
    ],
    partitions: 1
  },
  {
    active: true,
    serialized: true,
    // name_old: "votch_request_by_user_target",
    name: "-user-target@votchRequests",
    unique: true,
    source: "votchRequests",
    terms: [
      {
        field: ["data", "user"]
      },
      {
        field: ["data", "target"]
      }
    ],
    partitions: 1
  },
  {
    active: true,
    serialized: true,
    // name_old: "users_of_votch_requests_by_target",
    name: "-target@votchRequests+user",
    unique: false,
    source: "votchRequests",
    terms: [
      {
        field: ["data", "target"]
      }
    ],
    values: [
      {
        field: ["data", "user"]
      }
    ],
    partitions: 1
  },
  {
    active: true,
    serialized: true,
    // name_old: "user_fields_by_ref",
    name: "-ref@users+name+picture+username",
    unique: false,
    source: "users",
    terms: [
      {
        field: ["ref"]
      }
    ],
    values: [
      {
        field: ["data", "name"]
      },
      {
        field: ["data", "picture"]
      },
      {
        field: ["data", "username"]
      }
    ],
    partitions: 1
  },
  {
    active: true,
    serialized: true,
    // name_old: "notifs_by_owner",
    name: "-owner@notifs",
    unique: false,
    source: "notifs",
    terms: [
      {
        field: ["data", "owner"]
      }
    ],
    partitions: 1
  },
  {
    active: true,
    serialized: true,
    // name_old: "notifs_by_owner_and_seen",
    name: "-owner-seen@notifs",
    unique: false,
    source: "notifs",
    terms: [
      {
        field: ["data", "owner"]
      },
      {
        field: ["data", "seen"]
      }
    ],
    partitions: 1
  },
  {
    active: true,
    serialized: true,
    // name_old: "users_by_id",
    name: "-id@users+id+username+name+picture",
    unique: false,
    source: "users",
    terms: [
      {
        field: ["data", "id"]
      }
    ],
    values: [
      {
        field: ["data", "id"]
      },
      {
        field: ["data", "username"]
      },
      {
        field: ["data", "name"]
      },
      {
        field: ["data", "picture"]
      }
    ],
    partitions: 1
  },
  {
    active: true,
    serialized: true,
    // name_old: "usernames",
    name: "-username@users+username",
    unique: true,
    source: "users",
    terms: [
      {
        field: ["data", "username"]
      }
    ],
    values: [
      {
        field: ["data", "username"]
      }
    ],
    partitions: 1
  },
  {
    active: false,
    serialized: true,
    // name_old: "voters_info_by_ballot_id",
    name: "-ballot_id@votes+vote+user__id+user__username+user__name+user__picture+ts",
    unique: false,
    source: "votes",
    terms: [
      {
        field: ["data", "ballot_id"]
      }
    ],
    values: [
      {
        field: ["data", "vote"]
      },
      {
        field: ["data", "user", "id"]
      },
      {
        field: ["data", "user", "username"]
      },
      {
        field: ["data", "user", "name"]
      },
      {
        field: ["data", "user", "picture"]
      },
      {
        field: ["data", "ts"]
      }
    ],
    partitions: 1
  },
  {
    new: false,
    // name_old: "active_users",
    name: "-is_active@users",
    unique: false,
    serialized: true,
    source: "users",
    terms: [
      {
        field: ["data", "is_active"]
      }
    ]
  },
  {
    new: false,
    // name_old: "all_users",
    name: "@users",
    unique: false,
    serialized: true,
    source: "users"
  },
  {
    new: false,
    // name_old: "users_by_ref",
    name: "-ref@users",
    unique: true,
    serialized: true,
    source: "users",
    terms: [
      {
        field: ["ref"]
      }
    ]
  },
  {
    new : false,
    // name_old: "ballot_owners",
    name: "-ref@ballots+owners",
    unique: false,
    serialized: true,
    source: "ballots",
    terms: [
      {
        field: ["ref"]
      }
    ],
    values: [
      {
        field: ["data", "owner"]
      }
    ]
  },
  {
    // name_old: "votes_by_owner",
    name: "-owner@votes",
    unique: false,
    serialized: true,
    source: "votes",
    terms: [
      {
        field: ["data", "owner"]
      }
    ]
  },
  {
    // name_old: "all_votes",
    name: "@votes",
    unique: false,
    serialized: true,
    source: "votes"
  },
  {
    // name_old: "votchRequests_by_target_and_seen",
    name: "-target-seen@votchRequests",
    unique: false,
    serialized: true,
    source: "votchRequests",
    terms: [
      {
        field: ["data", "target"]
      },
      {
        field: ["data", "seen"]
      }
    ]
  },
  {
    // name_old: "all_votchRequests",
    name: "@votchRequests",
    unique: false,
    serialized: true,
    source: "votchRequests"
  },
  {
    // name_old: "all_users_fields",
    name: "@users+username+name+id+picture",
    unique: false,
    serialized: true,
    source: "users",
    values: [
      {
        field: ["ref"]
      },
      {
        field: ["data", "username"]
      },
      {
        field: ["data", "name"]
      },
      {
        field: ["data", "id"]
      },
      {
        field: ["data", "picture"]
      }
    ]
  },
  {
    name: "-email@pushsubs+pushSub",
    source: "pushsubs",
    terms: [
      {
        field: ["data", "userEmail"],
      },
    ],
    values: [
      {
        field: "ts"
      },
      {
        field: ["data", "pushSub"]
      }
    ],
  },
  {
    name: "-userRef@pushsubs+pushSub",
    source: "pushsubs",
    terms: [
      {
        field: ["data", "userRef"],
      },
    ],
    values: [
      {
        field: "ts"
      },
      {
        field: ["data", "pushSub"]
      }
    ],
  },
  {
    name: "@ballots+all_images_tags",
    source: "ballots",
    values: [
      {
        field: ["data", "all_images_tags"]
      }
    ],
  },
  {
    name: "-tag@ballots+ref",
    source: "ballots",
    terms: [
      {
        field: ["data", "all_images_tags"],
      },
    ],
    values: [
      {
        field: "ref"
      }
    ],
  },
];

module.exports = fireTheEngine;