const q = require('faunadb').query;

async function collections ( faunaClient ) {
  for (const document of documents) {
    await faunaClient.query(
      q.CreateCollection({
        name: document.name,
      })
    ).then(
      res => console.log(`${document.name} inserted`),
      err => console.log(err.toString())
    )
  }
};

const documents = [
  {
    name: 'ballots',
  },
  {
    name: 'coins',
  },
  {
    name: 'comments',
  },
  {
    name: 'credentials',
  },
  {
    name: 'deletedStuff',
  },
  {
    name: 'images',
  },
  {
    name: 'notifs',
  },
  {
    name: 'rels',
  },
  {
    name: 'reports',
  },
  {
    name: 'stats',
  },
  {
    name: 'users',
  },
  {
    name: 'votchRequests',
  },
  {
    name: 'votes',
  },
  {
    name: 'pushsubs',
  },
];


module.exports = collections;

