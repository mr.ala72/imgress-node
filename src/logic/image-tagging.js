const faunadb = require('faunadb');
const q = faunadb.query;
const nodeFetch = require("node-fetch").default;

function faunaClient ( req ) {
    const token = process.env.FAUNA_SECRET //req.query.token;
    return {
        client: new faunadb.Client({ 
            secret: token,
            domain: process.env.FAUNA_DOMAIN,
            port: process.env.FAUNA_PORT,
            scheme: process.env.FAUNA_SCHEME,
            timeout: 3000 
        })
    }
}

async function tagImages (req, res) {
    try {
        let _images;
        const { ballotId = '', images = [] } = req.query;
        try {
            _images = JSON.parse(images);
        } catch (error) {
            _images = images.split(',')
        }
        if(!ballotId || !Array.isArray(_images)) throw Error('bad request');
        // const baseUrl = 'https://cdn.imgress.com/images/';
        const baseUrl = process.env.IMAGE_CDN_URL_PREFIX + '/';
        let resultPromise = [];
        _images.forEach(imagePath => {
            const imageUrl = imagePath.startsWith('http')? imagePath :baseUrl + imagePath;
            console.log({imageUrl});
            const url = 'https://api.imagga.com/v2/tags?image_url=' + encodeURIComponent(imageUrl);
            resultPromise.push(
                nodeFetch(url, {
                    headers: {
                        "Authorization": process.env.IMAGGA_AUTH_KEY
                    }
                }).then(r=> {
                    if (r.ok) return r.json();
                    throw Error(r.text())
                })
            );
        });
        console.log('request sent to imagga: ' ,{_images, ballotId});
        const allImagesDetections = await Promise.all(resultPromise);
        let allTags = [];
        const refinedResult = allImagesDetections.map((apiResult, index) => ({
            image_id: _images[index],
            tags: apiResult.result.tags.map(detection => {
                if (detection.confidence > 20) {
                    const tag = detection.tag.en;
                    allTags.push(tag)
                    return `${tag}:${detection.confidence.toFixed(2)}`
                }                
            }).filter(_=>_)
        })
        );
        console.log('response from imagga: ', {allTags, ballotId});
        const { client } = faunaClient(req);
        await client.query(
            q.Update(q.Ref(q.Collection('ballots'), ballotId), {
                data: {
                    images_tags: refinedResult,
                    all_images_tags: allTags
                }
            })
        ).then( document => {
            console.log('fauna: ', document);
            res.send(document.data)
        }).catch(err=>console.log(err))
    } catch (error) {
        console.log(error);
        res.status(400).send(error)
    }
};


module.exports = tagImages;