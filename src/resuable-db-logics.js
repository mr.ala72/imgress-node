const faunadb = require('faunadb');
const q = faunadb.query;

const userRefByEmail = ( email ) => 
    q.Select(
        "ref",
        q.Get(q.Match(q.Index("-email@users"), email)),
        "mr.ala73@gmail.com"
    )
;
const safeGet = ( ref ) => 
    q.If(q.Exists(ref), q.Get(ref), { data: null })
;

const CountOfIndexByMatch = ( indexName, matchArgs ) => 
    q.Count(q.Match(q.Index(indexName), matchArgs))
;

const GetDocumentMatchOfIndex = ( indexName, matchArgs ) => 
    q.Get(q.Match(q.Index(indexName), matchArgs))
;
const failSafeContain = (str, targetStr) => q.If(
    q.IsString(str),
    q.ContainsStr(str, targetStr),
    false
);

module.exports = {
    userRefByEmail,
    safeGet,
    CountOfIndexByMatch,
    failSafeContain,
    GetDocumentMatchOfIndex
};