
async function getGoogleUser (req) {
    let idToken;
    try {
        idToken = req.query.token || req.headers["authorization"].split(" ")[1];
    } catch (error) {
        throw `No token provided`
    }
    const token = idToken;
    if(!token) throw `No token provided`; 
    let parts = token.split('.');
    // let headerBuf = new Buffer(parts[0], 'base64');
    let bodyBuf = Buffer.from(parts[1], 'base64');
    let body = JSON.parse(bodyBuf.toString());        
    // console.log(body.exp, Date.now()/1000)
    if(body.exp < Date.now()/1000){
        throw 'expired token'
    }
    if ( !body.email ) {
        body.email = body['http://imgress.com/verfied_email']
    }
    return body;
}

function normalizedList (response) {
    return {
        after: cursorIdExtractor(response.after),
        before: cursorIdExtractor(response.before),
        data: response.data.map(idExtractor)
    }
}

function cursorIdExtractor (arr) {
    if(!arr) return;
    return arr.map(a=>a.id)
}

function idExtractor (ref) {
    if(!ref) return;
    return ref.id
}

function getRandomArbitrary(min, max) {
    return Math.random() * (max - min) + min;
}

function noOpResponse (msg) {
    return {
        done: true,
        description: msg,
        status: "no-op"
    }
}

function unAuthenticated (error) {
    return {
        statusCode: 401,
        body: error.toString()
    }
}

function serverError (error) {
    console.log(error)
    return {
        statusCode: 569,
        body: `${JSON.stringify(error, null, 2)}`
    }
}

function successResponse (response, formatter) {
    const _response = formatter? formatter(response): response.data || response
    return {
        statusCode: 200,
        body: _response
    }
}

async function baseline ( faunaQuery, req, formatter ) {
    return (await getGoogleUser(req).then(
        (googleUser) => faunaQuery(googleUser).then((_)=>successResponse(_, formatter)),
        unAuthenticated
    ).catch(serverError))
}

module.exports = {
    getGoogleUser,
    cursorIdExtractor,
    idExtractor,
    getRandomArbitrary,
    baseline,
    unAuthenticated,
    normalizedList,
    serverError,
    successResponse,
    noOpResponse
}